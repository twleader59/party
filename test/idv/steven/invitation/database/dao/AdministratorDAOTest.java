package idv.steven.invitation.database.dao;

import static org.junit.Assert.*;

import java.util.Calendar;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import idv.steven.invitation.SystemConfig;
import idv.steven.invitation.database.JpaConfig;
import idv.steven.invitation.database.entity.Administrator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class, SystemConfig.class })
public class AdministratorDAOTest {
	@Inject
	private AdministratorDAO dao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() {
		Administrator admin = new Administrator();
		admin.setEmail("hi.steven@gmail.com");
		admin.setPassword("password");
		admin.setDepartment("資訊部");
		admin.setOccupation("小經辦");
		admin.setRoleId("A0001");
		admin.setStatus("Y");
		admin.setCtime(Calendar.getInstance().getTime());
		
		int count = dao.create(admin);
		assertEquals(1, count);
	}
}
