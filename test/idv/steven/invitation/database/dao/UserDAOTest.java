package idv.steven.invitation.database.dao;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.SystemConfig;
import idv.steven.invitation.database.JpaConfig;
import idv.steven.invitation.database.entity.Appointment;
import idv.steven.invitation.database.entity.Users;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class, SystemConfig.class })
//@Transactional
public class UserDAOTest {
	@Inject
	private UsersDAO dao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() {
		Users user = new Users();
		user.setEmail("mary98@gmail.com");
		user.setNickname("Mary");
		user.setBirthday("19550621");
		user.setCity("新竹市");
		user.setGender("F");
		user.setPassword("password");
		user.setCtime(Calendar.getInstance().getTime());
		
		int n = dao.create(user);
		assertEquals(1, n);
	}
	
	@Transactional
	//@Test
	public void findTalking() {
		Users users = dao.find("hi.steven@gmail.com");
		List<Appointment> talking = users.getAppointments();
		for(Appointment t:talking) {
			System.out.println("結果:" + t.toString());
		}
	}
}
