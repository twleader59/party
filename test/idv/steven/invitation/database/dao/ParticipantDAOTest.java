package idv.steven.invitation.database.dao;

import static org.junit.Assert.*;

import java.util.Calendar;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.SystemConfig;
import idv.steven.invitation.database.JpaConfig;
import idv.steven.invitation.database.entity.Appointment;
import idv.steven.invitation.database.entity.Participant;
import idv.steven.invitation.database.entity.Users;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class, SystemConfig.class })
//@Transactional
public class ParticipantDAOTest {
	@Inject
	private ParticipantDAO daoPart;
	
	@Inject
	private AppointmentDAO daoApp;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testCreate() {
		Participant p = new Participant();
		
		p.setAppointment_sysId(12L);
		p.setEmail("michael@gmail.com");
		p.setPermit("Y");
		p.setCtime(Calendar.getInstance().getTime());
		
		int count = daoPart.create(p);
		assertEquals(1, count);
	}
	
	@Transactional
	@Test
	public void testFindFromAppointment() {
		Appointment a = daoApp.find("hi.steven@gmail.com");
		
		for(Users u:a.getParticpants()) {
			System.out.println(u.toString());
		}
	}

}
