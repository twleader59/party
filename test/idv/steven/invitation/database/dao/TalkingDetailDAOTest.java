package idv.steven.invitation.database.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import idv.steven.invitation.SystemConfig;
import idv.steven.invitation.database.JpaConfig;
import idv.steven.invitation.database.entity.complex.TalkingDetail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class, SystemConfig.class })
public class TalkingDetailDAOTest {
	@Inject
	private TalkingDetailDAO dao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindByAppointment() {
		List<TalkingDetail> detail = dao.findByAppointment(12L);
		for(TalkingDetail d:detail) {
			System.out.println(d.toString());
		}
	}

}
