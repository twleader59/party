package idv.steven.invitation.database.dao;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Calendar;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.SystemConfig;
import idv.steven.invitation.database.JpaConfig;
import idv.steven.invitation.database.entity.Appointment;
import idv.steven.invitation.database.entity.Users;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JpaConfig.class, SystemConfig.class })
//@Transactional
public class AppointmentDAOTest {
	@Inject
	private AppointmentDAO daoApp;
	
	@Inject
	private UsersDAO daoUsers;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	//@Test
	public void testCreate() throws IOException {
		//讀檔
		Path path = Paths.get("E:/55047183.jpg");
		byte[] photo = Files.readAllBytes(path);

		//寫入DB
		Appointment appointment = new Appointment();
		
		appointment.setEmail("hi.steven@gmail.com");
		appointment.setName("聖誕大餐");
		appointment.setRestaurant_sysid(1L);
		appointment.setDescription("聖誕節找人聊天");
		appointment.setPhoto(photo);
		appointment.setPeople(2);
		appointment.setWillPay(350);
		appointment.setPayKind("A");
		appointment.setCancel("N");
		appointment.setHtime(Calendar.getInstance().getTime());
		appointment.setCtime(Calendar.getInstance().getTime());
		
		int count = daoApp.create(appointment);
		assertEquals(1, count);
	}
	
	@Transactional
	//@Test
	public void testFind() throws IOException {
		Appointment a = daoApp.find("hi.steven@gmail.com");
		
		Path pathTo = Paths.get("E:/testFind.jpg");
		Files.write(pathTo, a.getPhoto(), new OpenOption[] {StandardOpenOption.CREATE, StandardOpenOption.WRITE});
	}
	
	@Transactional
	@Test
	public void testRelation() throws IOException {
		Users u = daoUsers.find("hi.steven@gmail.com");
		for(Appointment a:u.getAppointments()) {
			System.out.println("結果: " + a.toString());
		}
	}
}
