package idv.steven.invitation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"idv.steven.invitation.database"})
public class SystemConfig  {

}
