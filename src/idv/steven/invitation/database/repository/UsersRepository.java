package idv.steven.invitation.database.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import idv.steven.invitation.database.entity.Users;

public interface UsersRepository extends PagingAndSortingRepository<Users, Long> {

}
