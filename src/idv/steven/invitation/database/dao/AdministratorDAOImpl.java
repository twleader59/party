package idv.steven.invitation.database.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.database.entity.Administrator;

@Repository
public class AdministratorDAOImpl implements AdministratorDAO {
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional
	@Override
	public int create(Administrator admin) {
		manager.persist(admin);
		
		return 1;
	}

	@Override
	public Administrator find(String email) {
		TypedQuery<Administrator> query = manager.createQuery("select a from Administrator a where email = :email", Administrator.class);
		query.setParameter("email", email);
		return query.getSingleResult();
	}

}
