package idv.steven.invitation.database.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.database.entity.Appointment;

@Repository
public class AppointmentDAOImpl implements AppointmentDAO {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	@Override
	public int create(Appointment appointment) {
		manager.persist(appointment);
		return 1;
	}

	@Override
	public Appointment find(String email) {
		return manager.createQuery("select a from Appointment a where a.email = :email", Appointment.class).setParameter("email", email).getSingleResult();
	}
}
