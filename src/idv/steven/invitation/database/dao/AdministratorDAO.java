package idv.steven.invitation.database.dao;

import idv.steven.invitation.database.entity.Administrator;

public interface AdministratorDAO {
	public int create(Administrator admin);
	public Administrator find(String email);
}
