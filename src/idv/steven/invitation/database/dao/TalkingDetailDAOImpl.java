package idv.steven.invitation.database.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.database.entity.complex.TalkingDetail;

@Repository
public class TalkingDetailDAOImpl implements TalkingDetailDAO {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	@Override
	public List<TalkingDetail> findByAppointment(Long id) {
		return manager.createQuery("select t from TalkingDetail t where t.appointment_sysId = :appointment_sysId", TalkingDetail.class)
			.setParameter("appointment_sysId", id).getResultList();
	}

}
