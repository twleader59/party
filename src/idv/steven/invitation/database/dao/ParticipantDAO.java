package idv.steven.invitation.database.dao;

import idv.steven.invitation.database.entity.Participant;

public interface ParticipantDAO {
	public int create(Participant participant);
}
