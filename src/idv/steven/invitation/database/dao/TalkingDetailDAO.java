package idv.steven.invitation.database.dao;

import java.util.List;

import idv.steven.invitation.database.entity.complex.TalkingDetail;

public interface TalkingDetailDAO {
	public List<TalkingDetail> findByAppointment(Long id);
}
