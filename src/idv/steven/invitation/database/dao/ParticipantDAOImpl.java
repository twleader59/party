package idv.steven.invitation.database.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.database.entity.Participant;

@Repository
public class ParticipantDAOImpl implements ParticipantDAO {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	@Override
	public int create(Participant participant) {
		manager.persist(participant);
		
		return 1;
	}

}
