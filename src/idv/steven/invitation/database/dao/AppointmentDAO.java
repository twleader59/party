package idv.steven.invitation.database.dao;

import idv.steven.invitation.database.entity.Appointment;

public interface AppointmentDAO {
	public int create(Appointment appointment);
	public Appointment find(String email);
}
