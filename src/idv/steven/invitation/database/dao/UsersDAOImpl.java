package idv.steven.invitation.database.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.invitation.database.entity.Users;

@Repository
public class UsersDAOImpl implements UsersDAO {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public int create(Users user) {
		manager.persist(user);
		return 1;
	}

	@Override
	public Users find(String email) {
		return manager.find(Users.class, email);
	}
}
