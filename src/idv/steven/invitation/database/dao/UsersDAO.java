package idv.steven.invitation.database.dao;

import idv.steven.invitation.database.entity.Users;

public interface UsersDAO {
	public int create(Users user);
	public Users find(String email);
}
