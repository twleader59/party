package idv.steven.invitation.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name="PARTICIPANT",
	uniqueConstraints = {
		@UniqueConstraint(columnNames = {"APPOINTMENT_SYSID", "EMAIL"})
	})
public class Participant implements Serializable {
	private static final long serialVersionUID = 1623326419659203618L;
	
	private long appointment_sysId;
	private String email;
	private String permit;
	private Date ctime;
	
	@Id
	@Column(name="APPOINTMENT_SYSID", unique=true, nullable=false)
	public long getAppointment_sysId() {
		return appointment_sysId;
	}
	public void setAppointment_sysId(long appointment_sysId) {
		this.appointment_sysId = appointment_sysId;
	}
	
	@Id
	@Column(name="EMAIL", unique=true, nullable=false, length=100)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="PERMIT", unique=false, nullable=true, length=1, columnDefinition="varchar2(1) DEFAULT 'N'")
	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}
	
	@Column(name="CTIME", unique=false, nullable=false, columnDefinition="DATE DEFAULT sysdate")
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	
	@Override
	public String toString() {
		return "Participant [appointment_sysId=" + appointment_sysId + ", email=" + email + ", permit=" + permit
				+ ", ctime=" + ctime + "]";
	}
}
