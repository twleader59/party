package idv.steven.invitation.database.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name="USERS",
	uniqueConstraints = {
		@UniqueConstraint(columnNames = "EMAIL")
	})
public class Users extends AbstractUser implements Serializable {
	private static final long serialVersionUID = 3965333920543479036L;
	
	private String phone1;
	private String phone2;
	private String nickname;
	private String city;
	private String gender;
	private String birthday;
	private String industry;
	private String description;
	private Date vtime;
	
	private List<Appointment> appointments;

	@Column(name="PHONE1", unique=false, nullable=true, length=10)
	public String getPhone1() {
		return phone1;
	}
	
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	
	@Column(name="PHONE2", unique=false, nullable=true, length=10)
	public String getPhone2() {
		return phone2;
	}
	
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	
	@Column(name="NICKNAME", unique=false, nullable=true, length=30)
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	@Column(name="CITY", unique=false, nullable=true, length=10)
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="GENDER", unique=false, nullable=true, length=1)
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Column(name="BIRTHDAY", unique=false, nullable=true, length=8)
	public String getBirthday() {
		return birthday;
	}
	
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	@Column(name="INDUSTRY", unique=false, nullable=true, length=3)
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	@Column(name="DESCRIPTION", unique=false, nullable=true, length=100)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="VTIME", unique=false, nullable=true)
	public Date getVtime() {
		return vtime;
	}

	public void setVtime(Date vtime) {
		this.vtime = vtime;
	}

	@OneToMany(mappedBy="users", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	@Override
	public String toString() {
		return "Users [phone1=" + phone1 + ", phone2=" + phone2 + ", nickname=" + nickname + ", city=" + city
				+ ", gender=" + gender + ", birthday=" + birthday + ", industry=" + industry + ", description="
				+ description + ", vtime=" + vtime + ", appointments=" + appointments + ", toString()="
				+ super.toString() + "]";
	}
}
