package idv.steven.invitation.database.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name="ADMINISTRATOR",
		uniqueConstraints = {
			@UniqueConstraint(columnNames = "EMAIL")
		})
public class Administrator extends AbstractUser implements Serializable {
	private static final long serialVersionUID = 4001989953339069879L;
	
	private String department;
	private String roleId;
	private String status;
	
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Administrotor [department=" + department + ", roleId=" + roleId + ", status=" + status + ", toString()="
				+ super.toString() + "]";
	}
}
