package idv.steven.invitation.database.entity.complex;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

import idv.steven.invitation.database.entity.Talking;
import idv.steven.invitation.database.entity.Users;

/*
@SqlResultSetMapping(
		name = "TalkingDetail",
		entities = {
			@EntityResult(
					entityClass = Talking.class,
					fields = {
						@FieldResult(name = "appointment_sysId", column = "appointment_sysId"),
						@FieldResult(name = "content", column = "content"),
						@FieldResult(name = "ctime", column = "ctime"),
						@FieldResult(name = "sysId", column = "sysId")
					}
				),
			@EntityResult(
					entityClass = Users.class,
					fields = {
							@FieldResult(name = "nickname", column = "nickname"),
							@FieldResult(name = "gender", column = "gender")
					}
				)
		})
		*/
public class TalkingDetail implements Serializable {
	private static final long serialVersionUID = 7870455683831097052L;

	private long appointment_sysId;
	private String content;
	private Date ctime;
	private Long sysId;
	private String nickname;
	private String gender;
	
	public long getAppointment_sysId() {
		return appointment_sysId;
	}
	public void setAppointment_sysId(long appointment_sysId) {
		this.appointment_sysId = appointment_sysId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	
	@Id
	public Long getSysId() {
		return sysId;
	}
	public void setSysId(Long sysId) {
		this.sysId = sysId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return "TalkingDetail [appointment_sysId=" + appointment_sysId + ", content=" + content + ", ctime=" + ctime
				+ ", sysId=" + sysId + ", nickname=" + nickname + ", gender=" + gender + "]";
	}
}
