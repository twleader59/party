package idv.steven.invitation.database.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(
	name="APPOINTMENT",
	uniqueConstraints = {
		@UniqueConstraint(columnNames = "SYSID")
	})
public class Appointment implements Serializable {
	private static final long serialVersionUID = -3781642274581820116L;
	
	private String email;
	private String name;
	private Long restaurant_sysid;
	private String description; 
	private byte[] photo;
	private Integer people;
	private Integer willPay;
	private String payKind;
	private String cancel;
	private Date htime;
	private Date ctime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "email", insertable = false, updatable = false)
	private Users users;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "PARTICIPANT",
		joinColumns = { @JoinColumn(name = "APPOINTMENT_SYSID") },
		inverseJoinColumns = { @JoinColumn (name = "EMAIL") })
	private List<Users> particpants;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_APPOINTMENT")
    @SequenceGenerator(name="SEQ_APPOINTMENT", sequenceName="SEQ_APPOINTMENT", initialValue=1, allocationSize=1)
	private Long sysid;
	
	public Long getSysid() {
		return sysid;
	}
	public void setSysid(Long sysid) {
		this.sysid = sysid;
	}
	
	@Column(name="EMAIL", unique=false, nullable=false, length=100)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="NAME", unique=false, nullable=true, length=30)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="RESTAURANT_SYSID", unique=false, nullable=true)
	public Long getRestaurant_sysid() {
		return restaurant_sysid;
	}
	public void setRestaurant_sysid(Long restaurant_sysid) {
		this.restaurant_sysid = restaurant_sysid;
	}
	
	@Column(name="DESCRIPTION", unique=false, nullable=true, length=500)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="PHOTO", unique=false, nullable=true)
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	
	@Column(name="PEOPLE", unique=false, nullable=true)
	public Integer getPeople() {
		return people;
	}
	public void setPeople(Integer people) {
		this.people = people;
	}
	
	@Column(name="WILLPAY", unique=false, nullable=true)
	public Integer getWillPay() {
		return willPay;
	}
	public void setWillPay(Integer willPay) {
		this.willPay = willPay;
	}
	
	@Column(name="PAYKIND", unique=false, nullable=true, length=2)
	public String getPayKind() {
		return payKind;
	}
	public void setPayKind(String payKind) {
		this.payKind = payKind;
	}
	
	@Column(name="CANCEL", unique=false, nullable=true, length=1)
	public String getCancel() {
		return cancel;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	
	@Column(name="HTIME", unique=false, nullable=true)
	public Date getHtime() {
		return htime;
	}
	public void setHtime(Date htime) {
		this.htime = htime;
	}
	
	@Column(name="CTIME", unique=false, nullable=true)
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Users getUsers() {
		return users;
	}
	
	public void setUsers(Users users) {
		this.users = users;
	}
	
	public List<Users> getParticpants() {
		return particpants;
	}
	public void setParticpants(List<Users> particpants) {
		this.particpants = particpants;
	}
	
	@Override
	public String toString() {
		return "Appointment [email=" + email + ", name=" + name + ", restaurant_sysid=" + restaurant_sysid
				+ ", description=" + description + ", people=" + people
				+ ", willPay=" + willPay + ", payKind=" + payKind + ", cancel=" + cancel + ", htime=" + htime
				+ ", ctime=" + ctime + ", sysid=" + sysid + "]";
	}
}
