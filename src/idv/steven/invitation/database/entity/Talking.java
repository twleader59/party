package idv.steven.invitation.database.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name="TALKING",
	uniqueConstraints = {
			@UniqueConstraint(columnNames = {"APPOINTMENT_SYSID", "EMAIL"})
	})
public class Talking implements Serializable {
	private static final long serialVersionUID = 2545910687607431199L;

	private long appointment_sysId;
	private String email;
	private String content;
	private Date ctime;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_TALKING")
    @SequenceGenerator(name="SEQ_TALKING", sequenceName="SEQ_TALKING", initialValue=1, allocationSize=1)
	private Long sysid;

	@Column(name="APPOINTMENT_SYSID", unique=true, nullable=false)
	public long getAppointment_sysId() {
		return appointment_sysId;
	}

	public void setAppointment_sysId(long appointment_sysId) {
		this.appointment_sysId = appointment_sysId;
	}

	@Column(name="EMAIL", unique=true, nullable=false, length=100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="CONTENT", unique=true, nullable=false, length=1000)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name="CTIME", unique=false, nullable=false, columnDefinition="DATE DEFAULT sysdate")
	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Long getSysid() {
		return sysid;
	}

	public void setSysid(Long sysid) {
		this.sysid = sysid;
	}
	
	
}
